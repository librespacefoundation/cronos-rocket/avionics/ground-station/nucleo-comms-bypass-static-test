# Nucleo-Comms-Bypass-Static-Test

This is a project that we will use for testing purposes only. It is a system that will connect to the RMC project via CAN bus. 
It will receive that data coming from the RMC board and send them with UART to out computer. This way, we will be able to create live graphs relating the progress of our test. 
